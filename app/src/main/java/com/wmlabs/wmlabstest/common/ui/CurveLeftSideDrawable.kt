package com.wmlabs.wmlabstest.common.ui

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Outline
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt

/**
 * Draws a curve on bottom-left of the drawable as well as on the top-left with help of flag
 * @param color Color of the background region
 * @param cornerRadiusPx radius of the curve
 * @param knockoutTop Flag to control the top-left curve draw
 */
open class CurveLeftSideDrawable(
    @ColorInt var color: Int,
    private val cornerRadiusPx: Int,
    private val knockoutTop: Boolean
) : Drawable() {

    private val paint = Paint().apply {
        isAntiAlias = true
    }

    val path = Path()

    override fun onBoundsChange(bounds: Rect?) {

        bounds?.also {
            val boundsF = RectF(it)
            computePath(boundsF)
        }
    }

    private fun computePath(bounds: RectF) {

        path.reset()

        path.moveTo(bounds.left, bounds.top)
        if (knockoutTop) {
            path.quadTo(bounds.left, bounds.top + cornerRadiusPx, bounds.left + cornerRadiusPx, bounds.top + cornerRadiusPx)
            path.lineTo(bounds.right, bounds.top + cornerRadiusPx)
        } else {
            path.lineTo(bounds.right, bounds.top)
        }
        path.lineTo(bounds.right, bounds.bottom)
        path.lineTo(bounds.left + cornerRadiusPx, bounds.bottom)
        path.quadTo(bounds.left, bounds.bottom, bounds.left, bounds.bottom - cornerRadiusPx)
        path.lineTo(bounds.left, bounds.top)

        path.close()
    }

    override fun draw(canvas: Canvas) {
        paint.style = Paint.Style.FILL
        paint.color = color
        canvas.drawPath(path, paint)
    }

    override fun getOutline(outline: Outline) {
        try {
            outline.setConvexPath(path)
            outline.alpha = 1f
        } catch (t: Throwable) {
            super.getOutline(outline)
        }
    }

    override fun getOpacity() = PixelFormat.TRANSLUCENT

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        // not supported
    }
}