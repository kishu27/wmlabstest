package com.wmlabs.wmlabstest.common.data

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.wmlabs.wmlabstest.AppContext

/**
 * A shared preferences manager utility class
 */
class SharedPreferencesManager private constructor(context: Context) {

    private val prefs: SharedPreferences

    init {
        prefs = context.getSharedPreferences(FNAME, Context.MODE_PRIVATE)
    }

    operator fun contains(key: String): Boolean {
        return prefs.contains(key)
    }

    fun getInt(key: String, defValue: Int): Int {
        return prefs.getInt(key, defValue)
    }

    @SuppressLint("ApplySharedPref")
    fun setInt(key: String, value: Int) {
        val editor = prefs.edit()
        editor.putInt(key, value)
        editor.commit()
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return prefs.getBoolean(key, defaultValue)
    }

    @SuppressLint("ApplySharedPref")
    fun setBoolean(key: String, value: Boolean) {
        val editor = prefs.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    @SuppressLint("ApplySharedPref")
    fun clear(key: String) {
        val editor = prefs.edit()
        editor.remove(key)
        editor.commit()
    }

    companion object {
        private const val FNAME = "com.wmlabs.wmlabstest"
        val instance = SharedPreferencesManager(AppContext)

        fun generateInstanceFrom(ctx: Context): SharedPreferencesManager {
            return SharedPreferencesManager(ctx)
        }
    }
}