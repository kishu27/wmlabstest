package com.wmlabs.wmlabstest.common.ui

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.wmlabs.wmlabstest.AppResources
import com.wmlabs.wmlabstest.R

/**
 * Offsets in negative to make recycler view items overlap
 */
class OverlapItemDecoration : RecyclerView.ItemDecoration() {

    private val overlapInset = -AppResources.getDimensionPixelSize(R.dimen.productList_itemMarginInset)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.set(0, overlapInset, 0, 0)
    }
}
