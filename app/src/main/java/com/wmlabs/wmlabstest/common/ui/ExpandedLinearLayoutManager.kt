package com.wmlabs.wmlabstest.common.ui

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wmlabs.wmlabstest.AppResources
import com.wmlabs.wmlabstest.R

/**
 * A linearlayout manager that reserves some more space for extra draw area. The curves on the back
 * of the recyclerview requires that the next view should be drawn and when the last item is near
 * the bottom of the screen, we require the recycler view to draw one offscreen view
 */
class ExpandedLinearLayoutManager(context: Context) : LinearLayoutManager(context, RecyclerView.VERTICAL, false) {
    companion object {
        const val MULTIPLIER = 4
    }

    override fun getExtraLayoutSpace(state: RecyclerView.State?): Int {
        val existingSpace = super.getExtraLayoutSpace(state)
        return existingSpace + AppResources.getDimensionPixelSize(R.dimen.productList_itemMarginInset) * MULTIPLIER
    }
}
