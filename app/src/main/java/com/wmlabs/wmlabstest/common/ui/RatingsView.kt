package com.wmlabs.wmlabstest.common.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.R.drawable
import java.security.InvalidParameterException

class RatingsView : LinearLayout {

    private var fullStarImage: Int = FULL_STAR_ID
        set(value) {
            field = value
            invalidate()
        }
    private var halfStarImage: Int = HALF_STAR_ID
        set(value) {
            field = value
            invalidate()
        }
    private var emptyStarImage: Int = EMPTY_STAR_ID
        set(value) {
            field = value
            invalidate()
        }

    var rating: Double = 0.0
        set(value) {
            if (value < 0 || value > MAX) {
                throw InvalidParameterException("Rating is out of range (0..5)")
            }

            field = value
            setStarValues()
            invalidate()
        }

    var starSize = DEFAULT_STAR_SIZE

    var starMargin = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {

        attrs?.also {
            val a = context.obtainStyledAttributes(it, com.wmlabs.wmlabstest.R.styleable.RatingsView, 0, 0)

            fullStarImage = a.getResourceId(R.styleable.RatingsView_fullStar, FULL_STAR_ID)
            halfStarImage = a.getResourceId(R.styleable.RatingsView_halfStar, HALF_STAR_ID)
            emptyStarImage = a.getResourceId(R.styleable.RatingsView_emptyStar, EMPTY_STAR_ID)

            rating = a.getFloat(R.styleable.RatingsView_value, 0f).toDouble()

            starSize = a.getDimensionPixelSize(R.styleable.RatingsView_starSize, DEFAULT_STAR_SIZE)

            starMargin = a.getDimensionPixelSize(R.styleable.RatingsView_starMargin, 0)

            a.recycle()
        }
    }

    private fun insertStars() {
        removeAllViews()

        for (i in 0 until MAX) {
            val img = ImageView(context)
            val lp = generateDefaultLayoutParams()
            lp.height = starSize
            lp.width = starSize
            img.layoutParams = lp
            img.setImageResource(emptyStarImage)
            lp.marginEnd = if (i == MAX - 1) 0 else starMargin

            addView(img)
        }
    }

    private fun setStarValues() {
        val starStates = getRatedValues(rating)
        for (i in 0..starStates.size) {
            getChildAt(i)?.also { imgView ->
                imgView as ImageView
                when (starStates[i]) {
                    VAL_EMPTY -> imgView.setImageResource(emptyStarImage)
                    VAL_HALF -> imgView.setImageResource(halfStarImage)
                    VAL_FULL -> imgView.setImageResource(fullStarImage)
                    else -> {
                        // No-op
                    }
                }
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        insertStars()
        setStarValues()
    }

    companion object {
        const val EMPTY_STAR_ID = drawable.ic_white_star_empty
        const val HALF_STAR_ID = drawable.ic_white_star_half
        const val FULL_STAR_ID = drawable.ic_white_star_full
        const val DEFAULT_STAR_SIZE = 76 // px
        const val MAX = 5

        const val VAL_EMPTY = 0
        const val VAL_HALF = 1
        const val VAL_FULL = 2

        fun getRatedValues(rating: Double): IntArray {
            val ar = IntArray(MAX)
            var r = rating

            for (i in 0 until MAX) {
                when {
                    r >= 1.0 -> {
                        ar[i] = VAL_FULL
                        r--
                    }
                    r > 0.0 -> {
                        ar[i] = VAL_HALF
                        r = 0.0
                    }
                    else -> ar[i] = VAL_EMPTY
                }
            }

            return ar
        }
    }
}