package com.wmlabs.wmlabstest.common.http

/**
 * Implementation of one API contract.
 *
 * ** IN a real project this will be a much better structure of
 * Exceptions or SealedClasses that will distribute the global states (network, common across all
 * API etc to a base class of ViewModel + View) and specific states (API contract states) only to
 * the views that are concerned with it
 */
enum class NetworkLoadState {
    /**
     * Data is loading or not loaded yet
     */
    LOADING,
    /**
     * Data is loaded
     */
    DONE,
    /**
     * Paginated data is loaded and no more left to fetch
     */
    DONE_END_OF_LIST,
    /**
     * Error as per API (4xx/5xx)
     */
    ERROR_API,
    /**
     * Internal networking error
     */
    ERROR_INTERNAL
}