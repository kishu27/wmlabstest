package com.wmlabs.wmlabstest.common.utils

import android.text.TextUtils
import android.util.Log
import com.wmlabs.wmlabstest.BuildConfig

/**
 * Safe Logging class. Responsibilities:
 *
 *  * If log tag is more than 23 chars, limits log tags to 23 characters and adds remaining substring to the message
 *  * Doesn't print a log for non-debug builds
 *  * If the caller class (passed as Class variable in each method) has a static boolean field named `DEBUG_LOG`, then its value is treated as a switch to logging
 *
 *
 * @author Ratul Kislaya
 */
object SafeLog {

    /**
     * Set to log the tag as "SafeLog".
     * This will make it easier to filter the app logs and filter out anything else that Android framework publishes under the application package name.
     *
     * The supplied tag from the caller will be a part of the Log message
     */
    private const val USE_CUSTOM_TAG = true

    private const val INDEX_TAG = 0
    private const val INDEX_MSG = 1

    private const val ELLIPSES = "…"
    private const val SEPARATOR = ": "

    /**
     * Finds if a class is permitting logging. Returns a Log Tag if logging can be done
     * @param clazz The caller class
     * @return Log tag | null if logging is not allowed
     */
    private fun getLogTagIfDebuggable(clazz: Class<*>): String? {

        // Only stop logging if class declares DEBUG_LOG = false;
        return if (!isDebugEnabled()) {
            null
        } else clazz.simpleName
    }

    private fun isDebugEnabled(): Boolean {
        return BuildConfig.DEBUG
    }

    /**
     * Send a [Log.VERBOSE] log message.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     */
    fun v(clazz: Class<*>, msg: String): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.v(bundle[INDEX_TAG], bundle[INDEX_MSG])
    }

    /**
     * Send a [Log.VERBOSE] log message and log the exception.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    fun v(clazz: Class<*>, msg: String, tr: Throwable): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.v(bundle[INDEX_TAG], bundle[INDEX_MSG], tr)
    }

    /**
     * Send a [Log.DEBUG] log message.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     */
    fun d(clazz: Class<*>, msg: String): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.d(bundle[INDEX_TAG], bundle[INDEX_MSG])
    }

    /**
     * Send a [Log.DEBUG] log message and log the exception.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    fun d(clazz: Class<*>, msg: String, tr: Throwable): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.d(bundle[INDEX_TAG], bundle[INDEX_MSG], tr)
    }

    /**
     * Send an [Log.INFO] log message.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     */
    fun i(clazz: Class<*>, msg: String): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.i(bundle[INDEX_TAG], bundle[INDEX_MSG])
    }

    /**
     * Send a [Log.INFO] log message and log the exception.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    fun i(clazz: Class<*>, msg: String, tr: Throwable): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.i(bundle[INDEX_TAG], bundle[INDEX_MSG], tr)
    }

    /**
     * Send a [Log.WARN] log message.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     */
    fun w(clazz: Class<*>, msg: String): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.w(bundle[INDEX_TAG], bundle[INDEX_MSG])
    }

    /**
     * Send a [Log.WARN] log message and log the exception.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    fun w(clazz: Class<*>, msg: String, tr: Throwable): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.w(bundle[INDEX_TAG], bundle[INDEX_MSG], tr)
    }

    /**
     * Send a [Log.ERROR] log message.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     */
    fun e(clazz: Class<*>, msg: String): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.w(bundle[INDEX_TAG], bundle[INDEX_MSG])
    }

    /**
     * Send a [Log.ERROR] log message and log the exception.
     *
     * @param clazz Source of LogTag (from Simple Name) and DEBUG_LOG boolean field
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    fun e(clazz: Class<*>, msg: String, tr: Throwable): Int {
        if (!BuildConfig.DEBUG) {
            return 0
        }

        val tag = getLogTagIfDebuggable(clazz) ?: return 0

        val bundle = makeLogSafe(tag, msg)
        return Log.w(bundle[INDEX_TAG], bundle[INDEX_MSG], tr)
    }

    /**
     * Takes tag and messages and makes it safe for logging with Android. If tag is more than 23 chars long,
     * a part of it will be prepended in the message
     *
     * @param tag
     * @param msg
     * <@UNVERIFIED|@return> Array of strings with log and message
     */
    private fun makeLogSafe(tag: String, msg: String?): Array<String> {
        val returnable = Array(2) { "" }

        @Suppress("ConstantConditionIf")
        if (USE_CUSTOM_TAG) {
            returnable[0] = SafeLog::class.java.simpleName
            returnable[1] = "$tag:$msg"
            return returnable
        }

        when {
            TextUtils.isEmpty(tag) -> {
                returnable[INDEX_TAG] = ""
                returnable[INDEX_MSG] = msg ?: ""
            }
            tag.length > 23 -> {
                val part1 = tag.substring(0, 22) + ELLIPSES
                val part2 = ELLIPSES + tag.substring(22)

                returnable[INDEX_TAG] = part1
                returnable[INDEX_MSG] = part2 + SEPARATOR + (msg ?: "")
            }
            else -> {
                returnable[INDEX_TAG] = tag
                returnable[INDEX_MSG] = msg ?: ""
            }
        }

        return returnable
    }
}
