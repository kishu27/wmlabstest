package com.wmlabs.wmlabstest.common.model

/**
 * Not used directly in UI, just to send paginated response to appropriate consumers
 */
data class ProductListResponse(
    val products: List<Product>,
    val totalProducts: Int,
    val pageNumber: Int,
    val pageSize: Int,
    val statusCode: Int
)
