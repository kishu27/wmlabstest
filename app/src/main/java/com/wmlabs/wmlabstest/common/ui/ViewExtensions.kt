package com.wmlabs.wmlabstest.common.ui

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.ImageView
import android.widget.TextSwitcher
import android.widget.TextView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.wmlabs.wmlabstest.R

/**
 * Sets HTML Text on TextView
 */
fun TextView.setWebserviceHtmlText(htmlText: String) {
    text = getHtmlSpan(htmlText)
}

/**
 * Sets HTML Text on TextView
 */
fun TextSwitcher.setWebserviceHtmlText(htmlText: String) {
    setText(getHtmlSpan(htmlText))
}

/**
 * Loads images on ImageViews
 */
fun ImageView.setNetworkImage(url: String) {

    val circularProgressDrawable = CircularProgressDrawable(context).apply {
        strokeWidth = 5f
        centerRadius = 30f
    }

    circularProgressDrawable.start()

    Glide.with(context)
        .load(url)
        .error(R.drawable.ic_out_of_stock_icon)
        .placeholder(circularProgressDrawable)
        .into(this)
}

private fun getHtmlSpan(html: String): Spanned? {

    val processThis = html.replace("\ufffd", "") // Remove REPLACEMENT_CHAR from service response

    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(processThis, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(processThis)
    }
}