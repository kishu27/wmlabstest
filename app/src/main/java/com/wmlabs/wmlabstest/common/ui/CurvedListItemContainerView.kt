package com.wmlabs.wmlabstest.common.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.Region
import android.graphics.drawable.RippleDrawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.LinearLayout

/**
 * A view that curves its left top and left bottom edges
 */
class CurvedListItemContainerView : LinearLayout {

    /**
     * Curve drawing handled via a custom drawable to avoid image overhead
     */
    private var bgDrawable: CurveLeftSideDrawable? = null

    /**
     * Separated background color from the curve, but its treated as a background
     */
    var curveBackgroundColor: Int = DEFAULT_BG_COLOR
        set(value) {
            field = value
            refreshColors()
            invalidate()
        }

    /**
     * Ripple color to handle the clicks within the boundaries
     */
    private var rippleColor: ColorStateList = ColorStateList.valueOf(DEFAULT_RIPPLE_COLOR)
        set(v) {
            field = v
            refreshColors()
            invalidate()
        }

    /**
     * Flag that draws curve on top too
     */
    private var knockoutTop: Boolean = DEFAULT_KNOCKOUT_TOP_FLAG
        set(v) {
            field = v
            refreshColors()
            invalidate()
        }

    /**
     * Radius of the curve
     */
    private var curveRadius: Int = DEFAULT_CURVE_RADIUS
        set(v) {
            field = v
            refreshColors()
            invalidate()
        }

    constructor(context: Context?) : super(context, null, DEFAULT_STYLE)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs, DEFAULT_STYLE) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        attrs?.also {
            val a = context.obtainStyledAttributes(it, com.wmlabs.wmlabstest.R.styleable.CurvedListItemContainerView, 0, DEFAULT_STYLE)

            rippleColor =
                a.getColorStateList(com.wmlabs.wmlabstest.R.styleable.CurvedListItemContainerView_rippleColor) ?: ColorStateList.valueOf(
                    DEFAULT_RIPPLE_COLOR
                )
            knockoutTop = a.getBoolean(com.wmlabs.wmlabstest.R.styleable.CurvedListItemContainerView_knockoutTop, DEFAULT_KNOCKOUT_TOP_FLAG)
            curveRadius =
                a.getDimensionPixelSize(com.wmlabs.wmlabstest.R.styleable.CurvedListItemContainerView_curveRadius, DEFAULT_CURVE_RADIUS)
            curveBackgroundColor =
                a.getColor(com.wmlabs.wmlabstest.R.styleable.CurvedListItemContainerView_curveBackgroundColor, DEFAULT_BG_COLOR)

            a.recycle()
        }
        refreshColors()
        setWillNotDraw(false)
    }

    private fun refreshColors() {
        bgDrawable = CurveLeftSideDrawable(curveBackgroundColor, curveRadius, knockoutTop)
        val rippleDrawable = RippleDrawable(rippleColor, bgDrawable, bgDrawable)

        background = rippleDrawable
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {

        // Interrupts the clicks outside of the curve and doesn't let the external clicks pass
        event?.also { e ->
            if (e.action == MotionEvent.ACTION_DOWN) {
                val x = e.x
                val y = e.y

                bgDrawable?.path?.also { path ->
                    if (!isPointWithin(path, x.toInt(), y.toInt())) {
                        return false
                    }
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        // Erase everything out of the curved area
        bgDrawable?.path?.also {
            canvas?.clipPath(it)
        }
        super.onDraw(canvas)
    }

    companion object {
        private const val DEFAULT_STYLE = com.wmlabs.wmlabstest.R.style.Widget_AppCompat_Button
        private const val DEFAULT_KNOCKOUT_TOP_FLAG = false
        private const val DEFAULT_BG_COLOR = 0xffff0000.toInt()
        private const val DEFAULT_RIPPLE_COLOR = 0x11ffffff
        private const val DEFAULT_CURVE_RADIUS = 100

        fun isPointWithin(path: Path, left: Int, top: Int): Boolean {
            val rectF = RectF()
            path.computeBounds(rectF, true)
            val region = Region()
            region.setPath(path, Region(rectF.left.toInt(), rectF.top.toInt(), rectF.right.toInt(), rectF.bottom.toInt()))

            return region.contains(left, top)
        }
    }
}