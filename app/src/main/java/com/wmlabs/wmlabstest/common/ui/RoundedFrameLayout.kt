package com.wmlabs.wmlabstest.common.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * Framelayout that cuts background and content in a circle
 */
class RoundedFrameLayout : FrameLayout {

    /**
     * This path is used to mask out the outer edges of a circle on this View
     */
    private var clipPath: Path = Path()

    constructor(context: Context) : super(context) {
        init() // Set our initialization
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init() // Set our initialization
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init() // Set our initialization
    }

    /**
     * Initialize our stuff
     */
    private fun init() {
        setWillNotDraw(false)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        clipPath.reset()

        clipPath.addCircle((w / 2).toFloat(), (h / 2).toFloat(), if (w < h) (w / 2).toFloat() else (h / 2).toFloat(), Path.Direction.CW)
    }

    override fun onDraw(canvas: Canvas) {

        // Erase everything out of our little circle in clipPath and hence create the real rounded QuickContactBadge
        canvas.clipPath(clipPath)

        // Do everything else that original badge does. Drawing of the overlay is also handled there
        super.onDraw(canvas)
    }
}