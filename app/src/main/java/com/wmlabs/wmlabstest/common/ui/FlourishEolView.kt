package com.wmlabs.wmlabstest.common.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.wmlabs.wmlabstest.R

/**
 * Draws a curved line meant to be a flourish for end of lists
 */
class FlourishEolView : View {

    lateinit var path: Path
    lateinit var paint: Paint
    lateinit var rect: Rect
    var maxThickness: Int = DEFAULT_THICKNESS
    var curveRadius: Int = DEFAULT_CURVE_RADIUS

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {

        path = Path()
        paint = Paint()
        paint.isAntiAlias = true
        attrs?.also {
            val a = context.obtainStyledAttributes(it, com.wmlabs.wmlabstest.R.styleable.FlourishEolView, 0, 0)

            paint.color = a.getColor(R.styleable.FlourishEolView_curveFillColor, DEFAULT_BG_COLOR)
            curveRadius = a.getDimensionPixelSize(R.styleable.FlourishEolView_radius, DEFAULT_CURVE_RADIUS)
            maxThickness = a.getDimensionPixelSize(R.styleable.FlourishEolView_maxThickness, DEFAULT_THICKNESS)

            a.recycle()
        } ?: run {
            paint.color = DEFAULT_BG_COLOR
            curveRadius = DEFAULT_CURVE_RADIUS
            maxThickness = DEFAULT_THICKNESS
        }

        paint.style = Paint.Style.FILL
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        rect = Rect(paddingLeft, paddingTop, w - paddingRight, h - paddingBottom)

        val halfThickness = maxThickness / 2
        val center = rect.top + halfThickness
        val left = rect.left.toFloat()

        val wf = w.toFloat()
        val hf = h.toFloat()
        val htf = halfThickness.toFloat()
        val ctf = curveRadius.toFloat()

        path.reset()
        path.moveTo(left, center - htf)
        path.lineTo(w - ctf, center - htf)
        path.quadTo(wf, center - htf, wf, hf)
        path.quadTo(wf, center + htf, wf - curveRadius, center + htf)
        path.lineTo(left, center + htf)
        path.lineTo(left, center - htf)
        path.close()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawPath(path, paint)
    }

    companion object {
        private const val DEFAULT_BG_COLOR = 0xffff0000.toInt()
        private const val DEFAULT_CURVE_RADIUS = 100
        private const val DEFAULT_THICKNESS = 15
    }
}