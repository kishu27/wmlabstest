package com.wmlabs.wmlabstest.common.model

import android.os.Parcel
import android.os.Parcelable

data class Product(
    val productId: String,
    val productName: String,
    val shortDescription: String?,
    val longDescription: String,
    val price: String,
    val productImage: String,
    val reviewRating: Double,
    val reviewCount: Int,
    val inStock: Boolean
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte()
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Product

        if (productId != other.productId) return false
        if (productName != other.productName) return false
        if (shortDescription != other.shortDescription) return false
        if (longDescription != other.longDescription) return false
        if (price != other.price) return false
        if (productImage != other.productImage) return false
        if (reviewRating != other.reviewRating) return false
        if (reviewCount != other.reviewCount) return false
        if (inStock != other.inStock) return false

        return true
    }

    override fun hashCode(): Int {
        var result = productId.hashCode()
        result = 31 * result + productName.hashCode()
        result = 31 * result + shortDescription.hashCode()
        result = 31 * result + longDescription.hashCode()
        result = 31 * result + price.hashCode()
        result = 31 * result + productImage.hashCode()
        result = 31 * result + reviewRating.hashCode()
        result = 31 * result + reviewCount
        result = 31 * result + inStock.hashCode()
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(productId)
        parcel.writeString(productName)
        parcel.writeString(shortDescription)
        parcel.writeString(longDescription)
        parcel.writeString(price)
        parcel.writeString(productImage)
        parcel.writeDouble(reviewRating)
        parcel.writeInt(reviewCount)
        parcel.writeByte(if (inStock) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Product> {
        override fun createFromParcel(parcel: Parcel): Product {
            return Product(parcel)
        }

        override fun newArray(size: Int): Array<Product?> {
            return arrayOfNulls(size)
        }
    }
}