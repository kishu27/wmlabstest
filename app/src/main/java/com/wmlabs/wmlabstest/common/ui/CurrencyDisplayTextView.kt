package com.wmlabs.wmlabstest.common.ui

import android.content.Context
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.RelativeSizeSpan
import android.text.style.SuperscriptSpan
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.common.utils.SafeLog
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

/**
 * Displays a TextView in the Currency  style . This allows different portions of the currency to be displayed in different text sizes
 */
class CurrencyDisplayTextView : AppCompatTextView {

    /**
     * Flag that enables/disables the display of currency symbol
     */
    private var displayCurrencySymbol: Boolean = false

    /**
     * Relative font size of currency symbol
     */
    private var currencySymbolFontSizeScale: Float = 0.toFloat()

    /**
     * Flag that enables/disables the display of comma separators
     */
    private var displayCommas: Boolean = false

    /**
     * Flag that enables/disables the display of cents
     */
    private var displayCents: Boolean = false

    /**
     * Relative font size of cents
     */
    private var centsFontSizeScale: Float = 0.toFloat()

    // Local flag to lock/unlock text formatting logic accidentally causing stack overflow
    private var unlocked: Boolean = false

    private var suffix: String? = null

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    /**
     * Initializes the config variables from XML attributes or sets the defaults
     *
     * @param context
     * @param attrs
     */
    private fun init(context: Context, attrs: AttributeSet?) {
        if (attrs == null) {
            initWithDefaultValues()
            return
        }

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.CurrencyDisplayTextView, 0, 0)
        displayCurrencySymbol = a.getBoolean(R.styleable.CurrencyDisplayTextView_displayCurrency, DEFAULT_DISPLAY_CURRENCY_SYMBOL)
        currencySymbolFontSizeScale = a.getFloat(R.styleable.CurrencyDisplayTextView_currencyFontSizeScale, DEFAULT_CURRENCY_SYMBOL_FONT_SIZE_SCALE)
        displayCommas = a.getBoolean(R.styleable.CurrencyDisplayTextView_displayCommas, DEFAULT_DISPLAY_COMMAS)
        displayCents = a.getBoolean(R.styleable.CurrencyDisplayTextView_displayCents, DEFAULT_DISPLAY_CENTS)
        centsFontSizeScale = a.getFloat(R.styleable.CurrencyDisplayTextView_centsFontSizeScale, DEFAULT_CENTS_FONT_SIZE_SCALE)
        suffix = a.getString(R.styleable.CurrencyDisplayTextView_suffix)
        a.recycle()

        // Force text change so that the values coming from the layout goes through the formatting process
        unlocked = true
        val txt = text
        if (txt != null && txt.isNotEmpty()) {
            text = txt
        }
    }

    /**
     * Initialize config variables with default values
     */
    private fun initWithDefaultValues() {
        displayCurrencySymbol = DEFAULT_DISPLAY_CURRENCY_SYMBOL
        currencySymbolFontSizeScale = DEFAULT_CURRENCY_SYMBOL_FONT_SIZE_SCALE
        displayCommas = DEFAULT_DISPLAY_COMMAS
        displayCents = DEFAULT_DISPLAY_CENTS
        centsFontSizeScale = DEFAULT_CENTS_FONT_SIZE_SCALE
        suffix = null
    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)

        if (text == null || text.isEmpty()) {
            return
        }

        SafeLog.v(CurrencyDisplayTextView::class.java, "onTextChanged:$text")

        if (!unlocked) {
            SafeLog.i(CurrencyDisplayTextView::class.java, "onTextChanged: Returning early because another text change is underway")
            return
        }

        unlocked = false

        val finalValueBuilder = SpannableStringBuilder()

        // Extract current text and strip non-numeric characters
        val strippedValue = stripNonNumericText(text.toString())
        val plainValue = java.lang.Double.parseDouble(strippedValue)

        // Remove cents if needed by config
        val intendedValue: Double = if (displayCents) plainValue else plainValue.toInt().toDouble()

        // Add formatting of comma and decimals as per config
        val format = if (displayCents)
            if (displayCommas) "###,###,###,###,##0.00" else "#0.00"
        else if (displayCommas) "###,###,###,###,##0" else "#0"
        val decimalFormat = DecimalFormat(format, DecimalFormatSymbols.getInstance(Locale.US))
        val strVal = decimalFormat.format(intendedValue)

        SafeLog.i(CurrencyDisplayTextView::class.java, "Formatted as: $strVal")

        val currencySymbol = if (displayCurrencySymbol) currencySymbol else ""
        if (currencySymbolFontSizeScale == 1f) {
            finalValueBuilder.append(currencySymbol)
        } else {
            // Add currency symbol in front and superscript it
            if (!TextUtils.isEmpty(currencySymbol)) {
                val currencySpannableString = SpannableString(currencySymbol)
                val currencySpan = TopAlignSuperscriptSpan(currencySymbolFontSizeScale, 8.0f)
                currencySpannableString.setSpan(currencySpan, 0, currencySymbol.length, 0)
                finalValueBuilder.append(currencySpannableString)
            }
        }

        // Start adding the currency value from formatted currency string
        val amountString = SpannableString(strVal)

        // Add cents value and superscript it as per the config
        if (displayCents && centsFontSizeScale != 1f) {
            val decimalLocation = strVal.lastIndexOf(currencyDecimalSeparator)
            amountString.setSpan(RelativeSizeSpan(0.0f), decimalLocation, decimalLocation + 1, 0)

            val decimalSpan = TopAlignSuperscriptSpan(centsFontSizeScale)
            amountString.setSpan(decimalSpan, decimalLocation + 1, amountString.length, 0)
        }

        // Build the final spannable and set it
        finalValueBuilder.append(amountString)
        if (!TextUtils.isEmpty(suffix)) {
            finalValueBuilder.append(suffix)
        }

        setText(finalValueBuilder)

        SafeLog.d(CurrencyDisplayTextView::class.java, "Setting value: $finalValueBuilder")

        // Unlock future operations
        unlocked = true
    }

    /**
     * Span that top aligns a scaled down text
     */
    private class TopAlignSuperscriptSpan
    /**
     * Top align a superscript
     *
     * @param shiftPercentage
     */
    internal constructor(shiftPercentage: Float) : SuperscriptSpan() {

        private var fontScale = 2
        private var shiftPercentage = 0f
        private var offset = 0f

        init {
            if (shiftPercentage > 0.0 && shiftPercentage < 1.0) {
                this.shiftPercentage = shiftPercentage
            }
        }

        /**
         * Top align a superscript and allow for error correction
         *
         * @param shiftPercentage
         * @param offset Error correction offset
         */
        internal constructor(shiftPercentage: Float, offset: Float) : this(shiftPercentage) {
            this.offset = offset
        }

        override fun updateDrawState(tp: TextPaint) {

            // original ascent
            val ascent = tp.ascent()

            // scale down the font
            tp.textSize = tp.textSize / fontScale

            // get the new font ascent
            val newAscent = tp.fontMetrics.ascent

            // move baseline to top of old font, then move down size of new font
            // adjust for errors with shift percentage
            tp.baselineShift += (ascent - ascent * shiftPercentage - (newAscent - newAscent * shiftPercentage) - offset).toInt()
        }

        override fun updateMeasureState(tp: TextPaint) {
            updateDrawState(tp)
        }
    }

    companion object {
    /*
     * Default config values
     */
        private const val DEFAULT_DISPLAY_CURRENCY_SYMBOL = true
        private const val DEFAULT_DISPLAY_COMMAS = true
        private const val DEFAULT_DISPLAY_CENTS = true
        private const val DEFAULT_CURRENCY_SYMBOL_FONT_SIZE_SCALE = 0.3f
        private const val DEFAULT_CENTS_FONT_SIZE_SCALE = 0.25f

        /**
         * @return Currency symbol as per locale
         */
        val currencySymbol: String
            get() {
                val currency = Currency.getInstance(Locale.getDefault())
                return currency.symbol
            }

        /**
         * @return Currency decimal separator as per locale
         */
        val currencyDecimalSeparator: Char
            get() {
                val nf = NumberFormat.getInstance()
                if (nf is DecimalFormat) {
                    val sym = nf.decimalFormatSymbols
                    return sym.decimalSeparator
                }
                return '.'
            }

        /**
         * Removes non-numeric chars
         */
        fun stripNonNumericText(input: String): String {
            return input.replace("[^\\d.]".toRegex(), "")
        }
    }
}
