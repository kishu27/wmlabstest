package com.wmlabs.wmlabstest.common.http

import com.wmlabs.wmlabstest.common.model.ProductListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductListService {

    @GET("/walmartproducts/{pageNumber}/{pageSize}")
    fun getProducts(
        @Path("pageNumber") pageNumber: Int,
        @Path("pageSize") pageSize: Int
    ): Call<ProductListResponse>
}