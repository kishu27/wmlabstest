package com.wmlabs.wmlabstest.common.http

import com.squareup.moshi.Moshi
import com.wmlabs.wmlabstest.BuildConfig
import com.wmlabs.wmlabstest.common.utils.SafeLog
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitClient {

    companion object {

        private val TAG_CLASS = RetrofitClient::class.java

        /**
         * Server base. Set to public var for testability. Tests change this variable before activity
         * starts so that it can be pointed to a mock
         */
        public var BASE_URL = BuildConfig.BASE_URL

        var client: Retrofit? = null
            get() {
                if (field == null) {
                    field = generateClient()
                }
                return field
            }

        private fun generateClient(): Retrofit {

            SafeLog.i(TAG_CLASS, "Generating retrofit client on " + BuildConfig.BASE_URL)

            val moshi: Moshi = Moshi.Builder()
                .build()

            val converterFactory = MoshiConverterFactory.create(moshi)

            return Retrofit.Builder().also {
                it.baseUrl(BASE_URL)
                it.addConverterFactory(converterFactory)
            }.build()
        }
    }
}
