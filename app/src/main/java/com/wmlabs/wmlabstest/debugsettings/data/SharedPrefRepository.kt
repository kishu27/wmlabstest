package com.wmlabs.wmlabstest.debugsettings.data

import com.wmlabs.wmlabstest.BuildConfig
import com.wmlabs.wmlabstest.common.data.SharedPreferencesManager

/**
 * Helper repo for sharedprefs
 */
object SharedPrefRepository {

    /**
     * Sets the page size to be fetched with each HTTP load. Will not work in release builds
     */
    public fun setPageSize(pageSize: Int) {
        if (BuildConfig.DEBUG) {
            SharedPreferencesManager.instance.setInt(KEY_PAGE_SIZE, pageSize)
        }
    }

    /**
     * Sets the flag to slow down network calls to be fetched with each HTTP load. Will not work in
     * release builds
     */
    public fun setSlowDownNetwork(shouldSlowDown: Boolean) {
        if (BuildConfig.DEBUG) {
            SharedPreferencesManager.instance.setBoolean(KEY_SLOW_DOWN, shouldSlowDown)
        }
    }

    /**
     * @return if Debug build: return the set page size or default | return default for release
     * builds
     */
    public fun getPageSize(): Int {

        return if (BuildConfig.DEBUG) {
            SharedPreferencesManager.instance.getInt(KEY_PAGE_SIZE, DEFAULT_PAGE_SIZE_NETWORK)
        } else {
            DEFAULT_PAGE_SIZE_NETWORK
        }
    }

    /**
     * @return if Debug build: return the set slowdown flag or default | return default for release
     * builds
     */
    public fun isSlowDownNetwork(): Boolean {
        return if (BuildConfig.DEBUG) {
            SharedPreferencesManager.instance.getBoolean(KEY_SLOW_DOWN, DEFAULT_SLOW_DOWN_NETWORK)
        } else {
            DEFAULT_SLOW_DOWN_NETWORK
        }
    }

    private const val DEFAULT_SLOW_DOWN_NETWORK = false
    private const val DEFAULT_PAGE_SIZE_NETWORK = 15

    private const val KEY_PAGE_SIZE = "PageSize"
    private const val KEY_SLOW_DOWN = "SlowDown"
}
