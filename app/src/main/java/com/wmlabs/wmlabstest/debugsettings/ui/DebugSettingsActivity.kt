package com.wmlabs.wmlabstest.debugsettings.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.CompoundButton
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.debugsettings.data.SharedPrefRepository
import kotlinx.android.synthetic.main.activity_debug_settings.*

class DebugSettingsActivity : AppCompatActivity() {

    class SharedPrefViewModel : ViewModel() {
        var slowDownNetwork: Boolean = SharedPrefRepository.isSlowDownNetwork()
            set(value) {
                field = value
                SharedPrefRepository.setSlowDownNetwork(value)
            }
        var pageSize: Int = SharedPrefRepository.getPageSize()
            set(value) {
                field = value
                SharedPrefRepository.setPageSize(value)
            }
    }

    private lateinit var viewModel: SharedPrefViewModel

    private val slowdownCheckboxChangeListener = CompoundButton.OnCheckedChangeListener { _, value ->
        viewModel.slowDownNetwork = value
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(SharedPrefViewModel::class.java)
        setContentView(R.layout.activity_debug_settings)

        txtDebugSettings_pageSize.text = viewModel.pageSize.toString()
        chkDebugSettings_slowdownNetwork.isChecked = viewModel.slowDownNetwork
        skDebugSettingspageSize.progress = viewModel.pageSize

        chkDebugSettings_slowdownNetwork.setOnCheckedChangeListener(slowdownCheckboxChangeListener)
        skDebugSettingspageSize.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                viewModel.pageSize = i
                txtDebugSettings_pageSize.text = i.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // No-op
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                // No-op
            }
        })
    }

    companion object {
        fun createInstance(ctx: Context): Intent {
            return Intent(ctx, DebugSettingsActivity::class.java)
        }
    }
}
