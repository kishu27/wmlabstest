package com.wmlabs.wmlabstest

import android.app.Application
import android.os.Build
import android.os.StrictMode

class App : Application() {

    override fun onCreate() {
        setStrictMode() // Move this after super.onCreate() if libraries have violations (before making the strict mode more forgiving
        super.onCreate()

        // store a reference to the Application class first
        instance = this
    }

    private fun setStrictMode() {
        if (!BuildConfig.DEBUG) {
            return
        }

        val threadPolicyBuilder: StrictMode.ThreadPolicy.Builder = StrictMode.ThreadPolicy.Builder()
        val vmPolicyBuilder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()

        // Build policies
        threadPolicyBuilder.detectDiskReads()
            .detectDiskWrites()
            .detectNetwork()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            threadPolicyBuilder.detectResourceMismatches()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            threadPolicyBuilder.detectUnbufferedIo()
        }

        vmPolicyBuilder.detectLeakedSqlLiteObjects()
            .detectLeakedClosableObjects()
            .detectActivityLeaks()
            .detectLeakedSqlLiteObjects()
            .detectLeakedRegistrationObjects()

        // Consequences
        threadPolicyBuilder.penaltyLog()
        vmPolicyBuilder.penaltyLog()

        // Set them
        StrictMode.setThreadPolicy(threadPolicyBuilder.build())
        StrictMode.setVmPolicy(vmPolicyBuilder.build())
    }

    companion object {
        lateinit var instance: App
            private set
    }
}