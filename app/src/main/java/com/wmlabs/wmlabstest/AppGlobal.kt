package com.wmlabs.wmlabstest

import android.content.Context
import android.content.res.Resources

/**
 * This file contains global lookups for Application-level objects such as [Resources] and [Context]. These should not be used
 * for inflating Views because they are not themed.
 */

/**
 * The [Resources] from the Application Context. Useful for getting resources that are not dependent on a theme,
 * such as strings and dimensions.
 */
val AppResources: Resources
    get() = AppContext.resources

/**
 * The [Application] Context. It is OK to use this anywhere and to hold references to it, since it survives for the entire
 * life of the process.
 */
val AppContext: Context
    get() = App.instance