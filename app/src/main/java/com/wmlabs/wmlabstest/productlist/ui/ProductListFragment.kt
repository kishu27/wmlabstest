package com.wmlabs.wmlabstest.productlist.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.wmlabs.wmlabstest.BuildConfig
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.common.http.NetworkLoadState
import com.wmlabs.wmlabstest.common.http.ProductListService
import com.wmlabs.wmlabstest.common.http.RetrofitClient
import com.wmlabs.wmlabstest.common.model.Product
import com.wmlabs.wmlabstest.common.ui.ExpandedLinearLayoutManager
import com.wmlabs.wmlabstest.common.ui.OverlapItemDecoration
import com.wmlabs.wmlabstest.common.utils.SafeLog
import com.wmlabs.wmlabstest.debugsettings.data.SharedPrefRepository
import com.wmlabs.wmlabstest.debugsettings.ui.DebugSettingsActivity
import com.wmlabs.wmlabstest.productdetail.ProductDetailsFragmentArgs
import com.wmlabs.wmlabstest.productlist.data.ProductListDataSourceFactory
import com.wmlabs.wmlabstest.productlist.data.ProductListDataSourceRepo
import kotlinx.android.synthetic.main.fragment_product_list.view.btn_productList_debugSettings
import kotlinx.android.synthetic.main.fragment_product_list.view.rv_productListView
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.eol_productList_footer_eolView
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.prg_productList_footer_progressBar
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.txt_productList_footer_errorApi
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.txt_productList_footer_errorNetwork

class ProductListFragment : Fragment(), ProductListAdapter.OnProductSelectedListener {

    class ProductListViewModel : ViewModel() {

        private val networkService = RetrofitClient.client!!.create(ProductListService::class.java)
        private val pageSize = SharedPrefRepository.getPageSize()
        private val productDataSourceFactory: ProductListDataSourceFactory

        var productList: LiveData<PagedList<Product>>

        init {
            productDataSourceFactory = ProductListDataSourceFactory(networkService)
            val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize)
                .setEnablePlaceholders(false)
                .build()
            productList = LivePagedListBuilder<Int, Product>(productDataSourceFactory, config).build()
        }

        fun getState(): LiveData<NetworkLoadState> =
            Transformations.switchMap<ProductListDataSourceRepo, NetworkLoadState>(productDataSourceFactory.productListDataSourceLiveData, ProductListDataSourceRepo::state)

        fun listIsEmpty(): Boolean {
            return productList.value?.isEmpty() ?: true
        }
    }

    private lateinit var viewModel: ProductListViewModel
    private lateinit var productListAdapter: ProductListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ProductListViewModel::class.java)

        initState()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter(view)

        if (BuildConfig.DEBUG) {
            view.btn_productList_debugSettings.visibility = View.VISIBLE
            view.btn_productList_debugSettings.setOnClickListener {
                context?.also { ctx ->
                    startActivity(DebugSettingsActivity.createInstance(ctx))
                }
            }
        } else {
            view.btn_productList_debugSettings.visibility = View.GONE
        }
    }

    private fun initAdapter(view: View) {
        productListAdapter = ProductListAdapter(this)
        with(view.rv_productListView as RecyclerView) {
            layoutManager = ExpandedLinearLayoutManager(context)
            adapter = productListAdapter
            addItemDecoration(OverlapItemDecoration())
        }

        viewModel.productList.observe(this, Observer {
            productListAdapter.submitList(it)
        })
    }

    override fun onProductSelected(product: Product) {
        SafeLog.w(ProductListFragment::class.java, "Product Clicked: ${product.productName}")
        val bundle = ProductDetailsFragmentArgs.Builder(product).build().toBundle()
        findNavController().navigate(R.id.action_openProductDetails, bundle)
    }

    private fun initState() {
        viewModel.getState().observe(this, Observer { state ->
            view?.apply {

                prg_productList_footer_progressBar.visibility = View.GONE
                txt_productList_footer_errorNetwork.visibility = View.GONE
                txt_productList_footer_errorApi.visibility = View.GONE
                eol_productList_footer_eolView.visibility = View.GONE

                if (viewModel.listIsEmpty()) {
                    when (state) {
                        NetworkLoadState.LOADING -> {
                            prg_productList_footer_progressBar.visibility = View.VISIBLE
                        }
                        NetworkLoadState.ERROR_API -> {
                            txt_productList_footer_errorApi.visibility = View.VISIBLE
                        }
                        NetworkLoadState.ERROR_INTERNAL -> {
                            txt_productList_footer_errorNetwork.visibility = View.VISIBLE
                        }
                        else -> {
                            // No-op
                        }
                    }
                }
            }
            if (!viewModel.listIsEmpty()) {
                productListAdapter.setState(state ?: NetworkLoadState.DONE)
            }
        })
    }
}
