package com.wmlabs.wmlabstest.productlist.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.wmlabs.wmlabstest.common.http.NetworkLoadState
import com.wmlabs.wmlabstest.common.http.ProductListService
import com.wmlabs.wmlabstest.common.model.Product
import com.wmlabs.wmlabstest.common.model.ProductListResponse
import com.wmlabs.wmlabstest.common.utils.SafeLog
import com.wmlabs.wmlabstest.debugsettings.data.SharedPrefRepository
import retrofit2.Response
import java.io.IOException

/**
 * Data source as repo: Fetches paginated product lists
 */
class ProductListDataSourceRepo(
    private val networkService: ProductListService
) : PageKeyedDataSource<Int, Product>() {

    var state: MutableLiveData<NetworkLoadState> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Product>) {

        SafeLog.i(TAG_CLASS, "Load Initial called")

        requestDataAndSetState(1, params.requestedLoadSize, FORWARD) { _, products, _ ->
            callback.onResult(products, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        SafeLog.i(TAG_CLASS, "Load After called")

        requestDataAndSetState(params.key, params.requestedLoadSize, FORWARD) { _, products, adjacentPage ->
            callback.onResult(products, adjacentPage)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        SafeLog.i(TAG_CLASS, "Load Before called")

        requestDataAndSetState(params.key, params.requestedLoadSize, BACKWARD) { _, products, adjacentPage ->
            callback.onResult(products, adjacentPage)
        }
    }

    /**
     * For making requests, processing them as per API contract and converting the response in a UI
     * state that app understands (abstracts the API contract and logic)
     *
     ** IN a real project this will be a much better structure of
     * Exceptions or SealedClasses that will distribute the global states (network, common across all
     * API etc to a base class of ViewModel + View) and specific states (API contract states) only to
     * the views that are concerned with it
     */
    private fun requestDataAndSetState(
        pageNumber: Int,
        pageSize: Int,
        direction: Int,
        exec: ((responseCode: Int?, products: List<Product>, adjacentPage: Int) -> Unit)
    ) {
        updateState(NetworkLoadState.LOADING)

        val response: Response<ProductListResponse>
        try {
            response = networkService.getProducts(pageNumber, pageSize).execute()
        } catch (e: IOException) {
            updateState(NetworkLoadState.ERROR_INTERNAL)
            SafeLog.e(TAG_CLASS, "Internal Error: ${e.message}")
            return
        }

        if (SharedPrefRepository.isSlowDownNetwork()) {
            Thread.sleep(5000)
        }

        when (response.code()) {
            in 200..299 -> {
                response.body()?.also { body ->
                    if (body.products.isEmpty()) {
                        SafeLog.d(TAG_CLASS, "Empty/No more data")
                        updateState(NetworkLoadState.DONE_END_OF_LIST)
                    } else {
                        SafeLog.i(TAG_CLASS, "Good data")
                        updateState(NetworkLoadState.DONE)
                        exec.invoke(response.code(), response.body()!!.products, response.body()!!.pageNumber + direction)
                    }
                } ?: run {
                    SafeLog.e(TAG_CLASS, "No body")
                    updateState(NetworkLoadState.ERROR_API)
                }
            }
            else -> {
                SafeLog.e(TAG_CLASS, "Non-200 code | ${response.code()}")
                updateState(NetworkLoadState.ERROR_API)
            }
        }
    }

    private fun updateState(state: NetworkLoadState) {
        this.state.postValue(state)
    }

    companion object {
        val TAG_CLASS: Class<ProductListService> = ProductListService::class.java

        const val FORWARD = 1
        const val BACKWARD = -1
    }
}
