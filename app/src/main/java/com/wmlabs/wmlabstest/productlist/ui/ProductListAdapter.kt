package com.wmlabs.wmlabstest.productlist.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.wmlabs.wmlabstest.AppResources
import com.wmlabs.wmlabstest.BuildConfig
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.common.http.NetworkLoadState
import com.wmlabs.wmlabstest.common.model.Product
import com.wmlabs.wmlabstest.common.ui.setNetworkImage
import com.wmlabs.wmlabstest.common.ui.setWebserviceHtmlText
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.eol_productList_footer_eolView
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.prg_productList_footer_progressBar
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.txt_productList_footer_errorApi
import kotlinx.android.synthetic.main.layout_product_list_footer_listitem.view.txt_productList_footer_errorNetwork
import kotlinx.android.synthetic.main.layout_product_list_listitem.view.img_product_list_thumbnail
import kotlinx.android.synthetic.main.layout_product_list_listitem.view.txt_product_list_productName
import kotlinx.android.synthetic.main.layout_product_list_listitem.view.txt_product_list_product_price
import kotlinx.android.synthetic.main.layout_product_list_listitem.view.txt_product_list_product_rating
import kotlinx.android.synthetic.main.layout_product_list_listitem.view.vw_product_list_container

class ProductListAdapter(private val productSelectedListener: OnProductSelectedListener) :
    PagedListAdapter<Product, RecyclerView.ViewHolder>(ProductDiffCallback) {

    private var state = NetworkLoadState.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == DATA_VIEW_TYPE)
            ProductListItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_product_list_listitem, parent, false))
        else
            ProductListFooterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_product_list_footer_listitem, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_VIEW_TYPE) {

            val color = AppResources.getColor(COLORS[position % COLORS.size], null)

            (holder as ProductListItemViewHolder).bind(getItem(position), color)
        } else {
            (holder as ProductListFooterViewHolder).bind(state)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    companion object {

        private const val DATA_VIEW_TYPE = 1
        private const val FOOTER_VIEW_TYPE = 2

        val ProductDiffCallback = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem == newItem
            }
        }

        private val COLORS = intArrayOf(
            R.color.wmlTest_recyclerView_background1,
            R.color.wmlTest_recyclerView_background2,
            R.color.wmlTest_recyclerView_background3,
            R.color.wmlTest_recyclerView_background4,
            R.color.wmlTest_recyclerView_background5
        )
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state != NetworkLoadState.DONE)
    }

    fun setState(state: NetworkLoadState) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    interface OnProductSelectedListener {
        fun onProductSelected(product: Product)
    }

    private open inner class ProductListItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            view.setOnClickListener {
                getItem(adapterPosition)?.also { product ->
                    productSelectedListener.onProductSelected(product)
                }
            }
        }

        fun bind(product: Product?, backgroundColor: Int) {
            product?.also {
                itemView.txt_product_list_productName.setWebserviceHtmlText(it.productName)
                itemView.txt_product_list_product_price.text = it.price
                itemView.vw_product_list_container.curveBackgroundColor = backgroundColor

                itemView.txt_product_list_product_rating.rating = it.reviewRating
                itemView.img_product_list_thumbnail.setNetworkImage(BuildConfig.BASE_URL + it.productImage)
            } ?: run {
                itemView.txt_product_list_productName.text = ""
                itemView.txt_product_list_product_price.text = ""
                itemView.vw_product_list_container.curveBackgroundColor = backgroundColor
                itemView.txt_product_list_product_rating.rating = 0.0
                itemView.img_product_list_thumbnail.setImageDrawable(null)
            }
        }
    }

    private open inner class ProductListFooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(status: NetworkLoadState?) {
            itemView.apply {
                prg_productList_footer_progressBar.visibility = View.GONE
                txt_productList_footer_errorNetwork.visibility = View.GONE
                txt_productList_footer_errorApi.visibility = View.GONE
                eol_productList_footer_eolView.visibility = View.GONE

                when (status) {
                    NetworkLoadState.DONE_END_OF_LIST -> {
                        eol_productList_footer_eolView.visibility = View.VISIBLE
                    }
                    NetworkLoadState.LOADING -> {
                        prg_productList_footer_progressBar.visibility = View.VISIBLE
                    }
                    NetworkLoadState.ERROR_API -> {
                        txt_productList_footer_errorApi.visibility = View.VISIBLE
                    }
                    NetworkLoadState.ERROR_INTERNAL -> {
                        txt_productList_footer_errorNetwork.visibility = View.VISIBLE
                    }
                    else -> {
                        // No-op
                    }
                }
            }
        }
    }
}
