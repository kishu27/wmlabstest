package com.wmlabs.wmlabstest.productlist.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.wmlabs.wmlabstest.common.http.ProductListService
import com.wmlabs.wmlabstest.common.model.Product

class ProductListDataSourceFactory(
    private val networkService: ProductListService
) : DataSource.Factory<Int, Product>() {

    val productListDataSourceLiveData = MutableLiveData<ProductListDataSourceRepo>()

    override fun create(): DataSource<Int, Product> {
        val productListDataSource = ProductListDataSourceRepo(networkService)
        productListDataSourceLiveData.postValue(productListDataSource)
        return productListDataSource
    }
}
