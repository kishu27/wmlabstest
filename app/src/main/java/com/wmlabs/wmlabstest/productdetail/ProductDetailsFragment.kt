package com.wmlabs.wmlabstest.productdetail

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.wmlabs.wmlabstest.BuildConfig
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.common.model.Product
import com.wmlabs.wmlabstest.common.ui.setNetworkImage
import com.wmlabs.wmlabstest.common.ui.setWebserviceHtmlText
import kotlinx.android.synthetic.main.fragment_product_details.btn_productDetail_descriptionToggle
import kotlinx.android.synthetic.main.fragment_product_details.txt_productDetail_descriptionTextSwitcher
import kotlinx.android.synthetic.main.fragment_product_details.view.btn_productDetail_descriptionToggle
import kotlinx.android.synthetic.main.fragment_product_details.view.img_productDetail_heroImage
import kotlinx.android.synthetic.main.fragment_product_details.view.rt_productDetail_ratings
import kotlinx.android.synthetic.main.fragment_product_details.view.txt_productDetail_descriptionTextSwitcher
import kotlinx.android.synthetic.main.fragment_product_details.view.txt_productDetail_price
import kotlinx.android.synthetic.main.fragment_product_details.view.txt_productDetail_ratingsTotal
import kotlinx.android.synthetic.main.fragment_product_details.view.txt_productDetail_stockStatus
import kotlinx.android.synthetic.main.fragment_product_details.view.txt_productDetail_title

class ProductDetailsFragment : Fragment() {

    class ProductDetailViewModel : ViewModel() {
        var product: Product? = null
        var expanded: Boolean? = null
    }

    private lateinit var viewModel: ProductDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProductDetailViewModel::class.java)
        if (viewModel.product == null) {
            viewModel.product = arguments!!.getParcelable("product")!!

            viewModel.expanded = if (viewModel.product!!.shortDescription == null) {
                null
            } else {
                false
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.wmlabs.wmlabstest.R.layout.fragment_product_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_productDetail_descriptionTextSwitcher.setFactory {
            LayoutInflater.from(context).inflate(com.wmlabs.wmlabstest.R.layout.view_textview_product_description, view.txt_productDetail_descriptionTextSwitcher, false)
        }
        txt_productDetail_descriptionTextSwitcher.animateFirstView = false

        updateProductDetails()
        view.btn_productDetail_descriptionToggle.setOnClickListener {
            if (viewModel.expanded != null) {
                viewModel.expanded = !viewModel.expanded!!
                updateDescriptionStatus()
            }
        }
    }

    private fun updateProductDetails() {

        view?.apply {
            viewModel.product?.also { product ->
                txt_productDetail_title.setWebserviceHtmlText(product.productName)
                txt_productDetail_price.text = product.price
                txt_productDetail_ratingsTotal.text = getString(com.wmlabs.wmlabstest.R.string.reviewTotalFormatter, product.reviewCount)
                rt_productDetail_ratings.rating = product.reviewRating
                txt_productDetail_stockStatus.text = if (product.inStock) getString(R.string.in_stock) else context.getString(R.string.out_of_stock)
                val img: Drawable = resources.getDrawable(if (product.inStock) R.drawable.ic_in_stock_icon else R.drawable.ic_out_of_stock_icon, context.theme)
                txt_productDetail_stockStatus.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)
                img_productDetail_heroImage.setNetworkImage(BuildConfig.BASE_URL + product.productImage)
            }
            updateDescriptionStatus()
        }
    }

    private fun updateDescriptionStatus() {
        viewModel.product?.also { product ->
            when {
                product.shortDescription == null -> {
                    btn_productDetail_descriptionToggle.visibility = View.INVISIBLE
                    txt_productDetail_descriptionTextSwitcher.setWebserviceHtmlText(product.longDescription)
                }
                else -> {
                    btn_productDetail_descriptionToggle.visibility = View.VISIBLE
                    if (viewModel.expanded == true) {
                        txt_productDetail_descriptionTextSwitcher.setInAnimation(context, R.anim.slide_right_in)
                        txt_productDetail_descriptionTextSwitcher.setOutAnimation(context, R.anim.slide_right_out)

                        btn_productDetail_descriptionToggle.text = getString(com.wmlabs.wmlabstest.R.string.show_less)
                        txt_productDetail_descriptionTextSwitcher.setWebserviceHtmlText(product.longDescription)
                    } else {
                        txt_productDetail_descriptionTextSwitcher.setInAnimation(context, R.anim.slide_left_in)
                        txt_productDetail_descriptionTextSwitcher.setOutAnimation(context, R.anim.slide_left_out)

                        btn_productDetail_descriptionToggle.text = getString(com.wmlabs.wmlabstest.R.string.show_more)
                        txt_productDetail_descriptionTextSwitcher.setWebserviceHtmlText(product.shortDescription)
                    }
                }
            }
        }
    }
}
