package com.wmlabs.wmlabstest.productlist.ui

import android.content.Intent
import android.test.InstrumentationTestCase
import android.test.suitebuilder.annotation.LargeTest
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.wmlabs.wmlabstest.R
import com.wmlabs.wmlabstest.common.http.RetrofitClient
import com.wmlabs.wmlabstest.util.TestHelper.getStringFromFile
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ProductListActivityTest : InstrumentationTestCase() {

    @get:Rule
    val activityRule = ActivityTestRule(ProductListActivity::class.java, true, false)
    private var server: MockWebServer = MockWebServer()

    @Before
    fun setup() {
        injectInstrumentation(androidx.test.platform.app.InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun testFirstPage() {
        val filename = "product_first_load.json"
        server = MockWebServer()
        server.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(getStringFromFile(instrumentation.context, filename))
        )
        server.start()
        RetrofitClient.BASE_URL = server.url("/").toString()

        activityRule.launchActivity(Intent())
        Thread.sleep(3000)

        onView(withSubstring("Lovely TV Console")).check(matches(isDisplayed()))
        activityRule.finishActivity()
        server.shutdown()
    }

    @Test
    fun testPErrorDisplay() {
        server = MockWebServer()
        server.enqueue(
            MockResponse()
                .setResponseCode(400)
        )
        server.start()
        RetrofitClient.BASE_URL = server.url("/").toString()

        activityRule.launchActivity(Intent())

        Thread.sleep(3000)

        onView(withSubstring("Server Error")).check(matches(isDisplayed()))
        activityRule.finishActivity()
    }

    @Test
    fun testPDP() {
        val filename = "product_first_load.json"
        server = MockWebServer()
        server.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(getStringFromFile(instrumentation.context, filename))
        )
        server.start()
        RetrofitClient.BASE_URL = server.url("/").toString()

        activityRule.launchActivity(Intent())
        Thread.sleep(3000)

        onView(withId(R.id.rv_productListView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, RecyclerViewAction.clickChildViewWithId(R.id.vw_product_list_container)))

        Thread.sleep(1000)

        onView(withId(R.id.txt_productDetail_title)).check(matches(withSubstring("Lovely TV Console")))
        onView(withId(R.id.txt_productDetail_price)).check(matches(withSubstring("949")))
        onView(withId(R.id.txt_productDetail_stockStatus)).check(matches(withSubstring("In stock")))

        activityRule.finishActivity()
        server.shutdown()
    }
}

class RecyclerViewAction {

    companion object {
        fun clickChildViewWithId(id: Int): ViewAction {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View>? {
                    return null
                }

                override fun getDescription(): String {
                    return "Click on a child view with specified id."
                }

                override fun perform(uiController: UiController, view: View) {
                    val v = view.findViewById(id) as View
                    v.performClick()
                }
            }
        }
    }
}