package com.wmlabs.wmlabstest.common.data

import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test

@SmallTest
class SharedPreferencesManagerTest {

    lateinit var sprefManager: SharedPreferencesManager

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().context
        sprefManager = SharedPreferencesManager.generateInstanceFrom(context)
        sprefManager.clear(KEY_INT)
        sprefManager.clear(KEY_BOOL)
    }

    @Test
    @Throws(Exception::class)
    fun put_and_get_Int() {
        val VAL = 91

        var v = sprefManager.getInt(KEY_INT, 0)
        assertEquals(v, 0)

        sprefManager.setInt(KEY_INT, VAL)
        v = sprefManager.getInt(KEY_INT, 0)

        assertEquals(v, VAL)
    }

    @Test
    @Throws(Exception::class)
    fun put_and_get_Bool() {
        val VAL = true

        var v = sprefManager.getBoolean(KEY_BOOL, false)
        assertEquals(v, false)

        sprefManager.setBoolean(KEY_BOOL, VAL)
        v = sprefManager.getBoolean(KEY_BOOL, false)

        // Verify that the received data is correct.
        assertEquals(v, VAL)
    }

    @After
    fun after() {
        sprefManager.clear(KEY_INT)
        sprefManager.clear(KEY_BOOL)
    }

    companion object {
        const val KEY_INT = "intTest"
        const val KEY_BOOL = "boolTest"
    }
}