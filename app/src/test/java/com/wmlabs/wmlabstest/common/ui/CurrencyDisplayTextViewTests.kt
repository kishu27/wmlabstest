package com.wmlabs.wmlabstest.common.ui

import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.Locale

@RunWith(RobolectricTestRunner::class)
class CurrencyDisplayTextViewTests {

    companion object {
        fun setup(locale: Locale) {
            Locale.setDefault(locale)
            val ctx = InstrumentationRegistry.getInstrumentation().context
            val res = ctx.resources
            val config = res.configuration
            config.setLocale(locale)
            ctx.createConfigurationContext(config)
        }
    }

    @Test
    fun testCurrencySymbol_testUS() {
        setup(Locale.US)
        assertEquals("$", CurrencyDisplayTextView.currencySymbol)
    }

    @Test
    fun testCurrencySymbol_testKorea() {
        setup(Locale.KOREA)
        assertEquals("￦", CurrencyDisplayTextView.currencySymbol)
    }

    @Test
    fun testCurrencySymbol_testUK() {
        setup(Locale.UK)
        assertEquals("£", CurrencyDisplayTextView.currencySymbol)
    }

    @Test
    fun testCurrencySymbol_testJapanNegation() {
        setup(Locale.JAPAN)
        assertNotEquals("$", CurrencyDisplayTextView.currencySymbol)
    }

    @Test
    fun testCurrencySymbol_testTaiwanNegation() {
        setup(Locale.TAIWAN)
        assertNotEquals("$", CurrencyDisplayTextView.currencySymbol)
    }

    @Test
    fun testCurrencySymbol_testItalyNegation() {
        setup(Locale.ITALY)
        assertNotEquals("$", CurrencyDisplayTextView.currencySymbol)
    }

    @Test
    fun testCurrencyDecimalSeparator() {
        setup(Locale.US)
        assertEquals('.', CurrencyDisplayTextView.currencyDecimalSeparator)
    }

    @Test
    fun testStripNonNumericText_testAllAlpha() {
        assertEquals(CurrencyDisplayTextView.stripNonNumericText("AAA"), "")
    }

    @Test
    fun testStripNonNumericText_testAllNum() {
        assertEquals(CurrencyDisplayTextView.stripNonNumericText("123"), "123")
    }

    @Test
    fun testStripNonNumericText_testAlphaNum() {
        assertEquals(CurrencyDisplayTextView.stripNonNumericText("ABC123"), "123")
    }

    @Test
    fun testStripNonNumericText_testNumAlpha() {
        assertEquals(CurrencyDisplayTextView.stripNonNumericText("321XYZ"), "321")
    }

    @Test
    fun testStripNonNumericText_testMixed() {
        assertEquals(CurrencyDisplayTextView.stripNonNumericText("A1Y3Z9"), "139")
    }

    @Test
    fun testStripNonNumericText_testAllAlphaFail() {
        assertNotEquals(CurrencyDisplayTextView.stripNonNumericText("AAA"), "AAA")
    }

    @Test
    fun testStripNonNumericText_testAlphaNumFail() {
        assertNotEquals(CurrencyDisplayTextView.stripNonNumericText("ABC123"), "ABC123")
    }

    @Test
    fun testStripNonNumericText_testNumAlphaFail() {
        assertNotEquals(CurrencyDisplayTextView.stripNonNumericText("321XYZ"), "321XYZ")
    }

    @Test
    fun testStripNonNumericText_testMixFail() {
        assertNotEquals(CurrencyDisplayTextView.stripNonNumericText("A1Y3Z9"), "A1Y3Z9")
    }

    @Test
    fun testStripNonNumericText_testMixFailLowercase() {
        assertNotEquals(CurrencyDisplayTextView.stripNonNumericText("A1Y3Z9"), "a1y3z9")
    }
}
