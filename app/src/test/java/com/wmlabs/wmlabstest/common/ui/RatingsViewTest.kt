package com.wmlabs.wmlabstest.common.ui

import com.wmlabs.wmlabstest.common.ui.RatingsView.Companion.VAL_EMPTY
import com.wmlabs.wmlabstest.common.ui.RatingsView.Companion.VAL_FULL
import com.wmlabs.wmlabstest.common.ui.RatingsView.Companion.VAL_HALF
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class RatingsViewTest {

    @Test
    fun testRatedValues_testZeroRating() {
        val rating = 0.0
        val expected = intArrayOf(VAL_EMPTY, VAL_EMPTY, VAL_EMPTY, VAL_EMPTY, VAL_EMPTY)
        assertRatedValuesEquals(rating, expected)
    }

    @Test
    fun testRatedValues_testSmallestRating() {
        val rating = 0.1
        val expected = intArrayOf(VAL_HALF, VAL_EMPTY, VAL_EMPTY, VAL_EMPTY, VAL_EMPTY)
        assertRatedValuesEquals(rating, expected)
    }

    @Test
    fun testRatedValues_testMidRating() {
        val rating = 3.0
        val expected = intArrayOf(VAL_FULL, VAL_FULL, VAL_FULL, VAL_EMPTY, VAL_EMPTY)
        assertRatedValuesEquals(rating, expected)
    }

    @Test
    fun testRatedValues_testFullRating() {
        val rating = 5.0
        val expected = intArrayOf(VAL_FULL, VAL_FULL, VAL_FULL, VAL_FULL, VAL_FULL)
        assertRatedValuesEquals(rating, expected)
    }

    @Test
    fun testRatedValues_testOverflownRating() {
        val rating = 91.0
        val expected = intArrayOf(VAL_FULL, VAL_FULL, VAL_FULL, VAL_FULL, VAL_FULL)
        assertRatedValuesEquals(rating, expected)
    }

    @Test
    fun testRatedValues_testRandomOut() {
        val rating = 0.1
        val expected = intArrayOf(VAL_HALF, VAL_FULL, VAL_EMPTY, VAL_EMPTY, VAL_EMPTY)
        assertRatedValuesNotEquals(rating, expected)
    }
    @Test
    fun testRatedValues_testRandomOut2() {
        val rating = 0.1
        val expected = intArrayOf(VAL_HALF, VAL_EMPTY, VAL_EMPTY, VAL_HALF, VAL_EMPTY)
        assertRatedValuesNotEquals(rating, expected)
    }

    private fun assertRatedValuesEquals(rating: Double, expected: IntArray) {
        assertTrue(expected contentEquals RatingsView.getRatedValues(rating))
    }

    private fun assertRatedValuesNotEquals(rating: Double, expected: IntArray) {
        assertFalse(expected contentEquals RatingsView.getRatedValues(rating))
    }
}