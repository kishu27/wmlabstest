# WM Labs Test

## Purpose

Demo to consume: https://mobile-tha-server.firebaseapp.com/

## Building, installation and running 

### Download

See: https://bitbucket.org/kishu27/wmlabstest/downloads/

### Build all the flavors

1. Clone the repository
2. Build by `./gradlew clean assembleDebug assembleQa` (Only debug and qa builds are specified for avoiding release builds. See below)

### Test

`./gradlew clean testQa connectedDebugAndroidTest`

#### Code Changes

Run `./gradlew clean ktlintFormat` before a git push.

### Build release flavor

To protect this app from leaking its release build secrets, following measures have been taken:

1. Release keystore file is not included in the repo
2. Release keystore passwords and alias are not stored in the repo  

To build the release flavor, `credentials.properties` file is needed with following properties:

1. `storeFile`: Path to the Keystore file
2. `storePassword`: Store password of Keystore file
3. `keyAlias`: Alias to build from
3. `keyPassword`: Alias password

You can also specify these as parameters in gradle command. 

### Installation

To install: run `./gradlew clean installDebug`

#### Prerequisites

1. Install SDK and ADB: https://www.xda-developers.com/quickly-install-adb/
2. Connect a device, enable developer mode and allow USB debugging: https://developer.android.com/studio/run/device

___

## Decisions

### Design

Design inspiration:

1. https://dribbble.com/shots/5690048-Social-Meet-Up-UI-Kit
2. https://dribbble.com/shots/5772908-Feeds-Cards-Social-Meet-Up-Kit/attachments

![Design Inspiration](https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/5d5e3f72907227.5bf85995c6e35.gif "Design inspiration")


This looked like a cool challenge to solve. The curves of list items was a moderately complex problem. Following features were implemented:

1. Used a custom drawable instead of any image to cut the shape
2. Completely avoids overdraw caused by image layering
3. Drawable takes care of ripples drawn within the right boundary
4. Click targets are set appropriately down to the pixel despite the view overlapping

### Architecture and Engineering notes

**Architectural Pattern**: MVVM (influenced by Android Architecture Components)

- MVVM is not fully demonstrated as there's a lack of view initiated commands to the lower levels
- Package structure follows high level features (screen/section) on the screen
- Single Activity Pattern inspired by Android Navigation component and Navigation component is used wherever possible
- Best practices of Android are implemented
    - From the get go, strict mode is applied so that problems surface easily
    - Android theming is implemented correctly. i.e: Proper definition of theme, styles and text appearance with no hardcoded strings left around
    - Build system is properly done. Gradle files are modularized, a debug keystore is bundled. Release secrets are handled appropriately
    - Text sizing follows OS sizes
- Performance over ease of implementation: All the curve drawing and view decoration are done by code instead of using static images/PNGs
- Lean views: Instead of using bloated open source libraries, lean views are written to do the concise thing needed. However, where a finished library was needed, they are used appropriately
- Kotlin features are used appropriately 

## Screens

**Data Loaded**

![Data Loaded](doc/img/home-screen-loaded.jpg)                                                         	

This is the screen that loads when app is first launched. To minimize the clutter and have ample whitespace, only a few data attributes are displayed.

The images present a problem in terms of UI colors because white background is baked in. To blend them into the UI, a circular viewport is used, drawn by a custom view that draws the circle and clips the drawing within.

The curve effect is created by overlapping the views (-ve margin on LayoutManager) and uses a custom Drawable to draw the shape. To avoid overdraw, the curve is drawn on both top and bottom. The contents of the layout gets clipped and any touches outside the curved area is passed-through. The custom drawable also takes care of drawing the ripple when rows are touched.

**Data Loading**

![Data Loading](doc/img/home-screen-loading.jpg)                                                      	

This is the loading screen that shows when app is launched for the first time and data is fetched (initial load). This is different from the pagination progress display.

**Load Error**

![Load Error](doc/img/home-screen-error.jpg)

A network error (airplane mode, bad network, bad route, ssl handshake issue etc) is handled like this.

**Page Loading**

![Page Loading](doc/img/home-screen-page-load-progress.jpg)

The display of progress while loading next page.           	|

**Page load error: internal**

![Page Load Error: Internal](doc/img/home-screen-page-load-error.jpg)

Error display for network issues (airplane mode, bad network, bad route, ssl handshake issue etc).           

**Page load error: Server**

![Page Load Error: Server](doc/img/home-screen-api-error.jpg)

An API related error (both 4xx and 5xx).          	

**Page load: end of list**

![Page Load: end of list](doc/img/home-screen-page-load-end-of-list.jpg)

UI State that determines the end of the list           

**Product Detail**

![Product Detail](doc/img/detail-screen-1.jpg)

Product detail screen

All the details available in the Product JSON schema are used. It displays:
- Product name
- Price (in a custom view)
- Ratings and no of sources
- In/Out of Stock status
- Short description

*Long description is revealed when `Show More` button is clicked.

**Product Detail: Expanded Description**

![Product Detail: Expanded Description](doc/img/detail-screen-expanded-2.jpg) 

Screen state that displays the full description.        

**Debug Settings**

![Debug Settings](doc/img/debug-settings.jpg)

Bonus: Debug Settings.

By default, the app is tuned to load data silently in the background and in conjunction with fast load speeds, scrolling across pages seems seamless. To reveal some of the UI states, these settings can be used.     
